import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        // is not restarted.
        primarySwatch: Colors.green,
      ),
      home: MainSreen(),
    );
  }
}

class MainSreen extends StatefulWidget {
  @override
  _MainSreenState createState() => _MainSreenState();
}

class _MainSreenState extends State<MainSreen> {
  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    return Scaffold(
        appBar: AppBar(
          title: Text("Flutter"),
        ),
        body: Column(
          children: <Widget>[
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                SizedBox(
                  width: width-300,
                  height: 100,
                  child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      //per kolone, cross per rresht
                      children: <Widget>[
                        Text(
                          "HELLO ",
                          style: TextStyle(fontSize: 25, color: Colors.green),
                        ),
                      ]),
                ),
                SizedBox(
                  width: 100,
                  height: 100,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Text("WORLD",
                        style: TextStyle(fontSize: 25, color: Colors.green),),
                    ],
                  ),
                ),

              ],
            ),
            Row(
              children: <Widget>[
                SizedBox(
                  width: width,
                  child: Image.asset("assets/images/nature2.jpg"),

                )
              ],

            ),
            Padding(
              padding: const EdgeInsets.fromLTRB(0,10,0,10),
              child: Row(
                children: <Widget>[
                  SizedBox(
                    width: width/2,
                    child: Text(
                        "Reverse Squares in Nature",
                      style: TextStyle(fontSize: 25, color: Colors.green),
                    ),
                  ),
                  SizedBox(
                    width: width/2,
                      child: Image.asset("assets/images/1.jpg"),
    )

                ],
              ),
            )
          ],
        ),

    );
  }
}
